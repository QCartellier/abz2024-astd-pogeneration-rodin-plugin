# ASTD-PO-Generation-Rodin-Plugin

## Description

This project is related to the paper "A Rodin Plugin for Generating Proof Obligations for Invariant Preservation for ASTDs" submitted in ABZ 2025.
This project contains:
- the Rodin plugin as a .jar file
- the Java code of the plugin in the directory "ASTDContextPlugin"
- examples to test the plugin in "Examples"
- the diagram class of the ASTD metamodel in "diagram_class_astd.pdf". The ASTD metamodel corresponds to the one used in the plugin.

## Installation

To install the plugin, please download the .jar file at the root of the project. Then put the .jar file in the directory "rodin/plugins" where Rodin is installed on your machine.

## Plugin Usage

When Rodin is open, create a new Event-B project. Then, within the Project Explorer, add the specification JSON file in the new project directory. In the Project Explorer, select the file and right-click. Then in the menu, select "ASTD Context > From json ASTD to EventB Context". The Rodin context will be automatically generated.
Status on the generation will be printed in the console "CONSOLE_Plugin_ASTD_PO_Generation" during the execution.

## Examples

In examples, JSON files correspond to the specification of an ASTD. The associated PNG corresponds to the graphical representation of the specified ASTD. The associated PDF corresponds to the generated Rodin context.

## Contributors and Acknowledgement

The paper has been written in collaboration with Marc Frappier and Amel Mammar.

The authors would like to thank Idir Ait Sadoune, David Gelessus and Laurent Voisin for their help in creating the plugin.